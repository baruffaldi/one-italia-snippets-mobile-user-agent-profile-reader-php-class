<?php
/**
** Classe per l'identificazione del client e delle specifiche tecniche sullo stesso
**
** @Version 0.1a
** @SubPackage class.xml2array.php
** @Example $_CLIENT = new _client;
** @Uses AUTOMATED: Templates dimensions; Applications capabilities; AdServer banner dimensions;
** @ToDo Upload capabilities; Streaming capabilities;
** @Author Filippo Baruffaldi
** @Email filippo.baruffaldi@oneitalia.it
**
*/

include 'class.dump.php';
include 'class.xml2array.php';

$_CLIENT = array();

class _client {

			var $_client_ay			= array();
		
			function _client () {
					
					global $_CLIENT;
		
					//$URL					= "test.xml";
					$URL 						= $_SERVER['HTTP_X_WAP_PROFILE'];
					
					//*** Opening XML
					$fstream			= fopen( realpath( $URL ), 'r' );
					$fdump				= fread( $fstream, filesize( realpath( $URL ) ) );
					fclose( $fstream );
					
					//*** Creating an array that contains the XML structure
					$xmlObj				= new xml2Array( );
					$xmlArray 		= $xmlObj->parse( $fdump );
					$tmp_ay				= $this->recognizeDevice( );
					
					//*** Let's obtain all useful informations about the client device
					$this->_client_ay['IP']											= $_SERVER['REMOTE_ADDR'];
					$this->_client_ay['MSISDN']									= $_SERVER['HTTP_X_H3G_MSISDN'];
					$this->_client_ay['PARTYID']								= $_SERVER['HTTP_X_H3G_PARTY_ID'];
					$this->_client_ay['AGENT']									= $_SERVER['HTTP_USER_AGENT'];
					$this->_client_ay['CONTENT']								= $_SERVER['HTTP_ACCEPT'];
					$this->_client_ay['CHARSET_ACCEPT']					= $_SERVER['HTTP_ACCEPT_CHARSET'];
					$this->_client_ay['INPUT_CHARSET_ACCEPT']		= $this->getFromArray( "tagData", $xmlArray[0]['children'][0]['children'][0]['children'][0]['children'][3]['children'][0]['children'] );
					$this->_client_ay['OUTPUT_CHARSET_ACCEPT']	= $this->getFromArray( "tagData", $xmlArray[0]['children'][0]['children'][0]['children'][0]['children'][12]['children'][0]['children'] );
					$this->_client_ay['CCPP_CHARSET_ACCEPT']		= $this->getFromArray( "tagData", $xmlArray[0]['children'][0]['children'][1]['children'][0]['children'][3]['children'][0]['children'] );
					$this->_client_ay['CCPP_ENCODE']						= $this->getFromArray( "tagData", $xmlArray[0]['children'][0]['children'][1]['children'][0]['children'][4]['children'][0]['children'] );
					$this->_client_ay['DEVICE']									= $tmp_ay['name'];
					$this->_client_ay['DEVICE_VERSION']					= $tmp_ay['version'];
					$this->_client_ay['DEVICE_TYPE']						= $tmp_ay['type'];
					$this->_client_ay['DEVICE_MANUFACT']				= $tmp_ay['manufact'];
					$this->_client_ay['DEVICE_MODEL']						= $tmp_ay['model'];
					$this->_client_ay['SYSTEM']									= $this->recognizeSystem();
					$this->_client_ay['BEARERS']								= $this->getFromArray( "tagData", $xmlArray[0]['children'][0]['children'][2]['children'][0]['children'][2]['children'][0]['children'] );
					$this->_client_ay['SECURITY']								= $this->getFromArray( "tagData", $xmlArray[0]['children'][0]['children'][2]['children'][0]['children'][1]['children'][0]['children'] );
					$this->_client_ay['WAP_DEVICE_CLASS']				= $xmlArray[0]['children'][0]['children'][4]['children'][0]['children'][1]['tagData'];
					$this->_client_ay['WAP_VERSION']						= $xmlArray[0]['children'][0]['children'][4]['children'][0]['children'][2]['tagData'];
					$this->_client_ay['WML_VERSION']						= $this->getFromArray( "tagData", $xmlArray[0]['children'][0]['children'][4]['children'][0]['children'][3]['children'][0]['children'] );
					$this->_client_ay['WML_SCRIPT_VERSION']			= $this->getFromArray( "tagData", $xmlArray[0]['children'][0]['children'][4]['children'][0]['children'][5]['children'][0]['children'] );
					$this->_client_ay['WML_SCRIPT_LIB']					= $this->getFromArray( "tagData", $xmlArray[0]['children'][0]['children'][4]['children'][0]['children'][6]['children'][0]['children'] );
					$this->_client_ay['WTAI_LIB']								= $this->getFromArray( "tagData", $xmlArray[0]['children'][0]['children'][4]['children'][0]['children'][7]['children'][0]['children'] );
					$this->_client_ay['BROWSER']								= $xmlArray[0]['children'][0]['children'][3]['children'][0]['children'][1]['tagData'];
					$this->_client_ay['JAVA_ENABLED']						= $xmlArray[0]['children'][0]['children'][1]['children'][0]['children'][5]['tagData'];
					$this->_client_ay['JAVA_PLATFORM']					= $this->getFromArray( "tagData", $xmlArray[0]['children'][0]['children'][1]['children'][0]['children'][6]['children'][0]['children'] );
					$this->_client_ay['MMS_VERSION']						= $xmlArray[0]['children'][0]['children'][6]['children'][0]['children'][6]['children'][0]['children'][0]['tagData'];
					$this->_client_ay['MMS_MAXMSGSIZE']					= $xmlArray[0]['children'][0]['children'][6]['children'][0]['children'][1]['tagData'];
					$this->_client_ay['MMS_MAXIMGRESOLUTION']		= $xmlArray[0]['children'][0]['children'][6]['children'][0]['children'][2]['tagData'];
					$this->_client_ay['MMS_CONTENT_ACCEPT']			= $this->getFromArray( "tagData", $xmlArray[0]['children'][0]['children'][6]['children'][0]['children'][3]['children'][0]['children'] );
					$this->_client_ay['MMS_CHARSET_ACCEPT']			= $this->getFromArray( "tagData", $xmlArray[0]['children'][0]['children'][6]['children'][0]['children'][4]['children'][0]['children'] );
					$this->_client_ay['MMS_ENCODING_ACCEPT']		= $this->getFromArray( "tagData", $xmlArray[0]['children'][0]['children'][6]['children'][0]['children'][5]['children'][0]['children'] );
					$this->_client_ay['JAVA_APPLET_ENABLED']		= $xmlArray[0]['children'][0]['children'][3]['children'][0]['children'][5]['tagData'];
					$this->_client_ay['JAVASCRIPT']							= $xmlArray[0]['children'][0]['children'][3]['children'][0]['children'][6]['tagData'];
					$this->_client_ay['DOWNLOAD_CAPABLE']				= $xmlArray[0]['children'][0]['children'][1]['children'][0]['children'][1]['tagData'];
					$this->_client_ay['TABLE_CAPABLE']					= $xmlArray[0]['children'][0]['children'][3]['children'][0]['children'][4]['tagData'];
					$this->_client_ay['FRAME_CAPABLE']					= $xmlArray[0]['children'][0]['children'][3]['children'][0]['children'][2]['tagData'];
					$this->_client_ay['CONTENT_ACCEPT']					= $this->getFromArray( "tagData", $xmlArray[0]['children'][0]['children'][1]['children'][0]['children'][2]['children'][0]['children'] );
					$this->_client_ay['RESOLUTION']							= $xmlArray[0]['children'][0]['children'][0]['children'][0]['children'][1]['tagData'];
					$this->_client_ay['WIDTH']									= $this->getDimension( "width", $xmlArray[0]['children'][0]['children'][0]['children'][0]['children'][1]['tagData'] );
					$this->_client_ay['HEIGHT']									= $this->getDimension( "height", $xmlArray[0]['children'][0]['children'][0]['children'][0]['children'][1]['tagData'] );
					$this->_client_ay['TAGLIO_BANNER']					= $this->_client_ay['WIDTH'] - 3;
					$this->_client_ay['TAGLIO_NOWLET']					= $this->_client_ay['WIDTH'] / 2 - 1;
					//$this->_client_ay['COOKIES']					= $xmlArray[0]['children'][0]['children'][0]['children'][0]['children'];
					
					$_CLIENT 																		= $this->checkFaults( $this->_client_ay );
					$GLOBALS['_CLIENT']													= $_CLIENT;
					
					return $_CLIENT;
			}

	/**
     * Funzione per controllare se il client sta usando un determinato device.
     *
     * @param string $device Nome del device da controllare
     * @return boolean
     */	
	
			function gotDevice ($device) {
				$ret = (preg_match('/'.$device.'/si', $_SERVER['HTTP_USER_AGENT'])) ? true : false;
				
				return $ret;
			}

	/**
     * Funzione per controllare se il client supporta lo streaming.
     *
     * @param string $device Nome del device da controllare
     * @return boolean
     */	
	
			function gotStreaming () {
				$key = array_search('Streaming', $this->_client_ay);
				_dump($key);
				_dump($this->_client_ay[$key]);
				
				$ret = (array_search('Streaming', $this->_client_ay)) ? true : false ;
				
				return $ret;
			}
			
	/**
     * Funzione per controllare se il client sta usando un determinato sistema.
     *
     * @param string $system Nome del sistema da controllare
     * @return boolean
     */	
	
			function gotSysten ($system) {
				$ret = (preg_match('/'.$system.'/si', $_SERVER['HTTP_USER_AGENT'])) ? true : false;
				
				return $ret;
			}
			
	/**
     * Funzione per ottenere width o height da una risoluzione
     *
     * @param  side string (width|height)
     * @param  resolution string NNNxNNN
     * @return string
     * 
     */	
			function getDimension ( $side, $resolution ) {
				
				$tmpArray = explode("x", $resolution);
				
				if ($side == "width")
						return $tmpArray[0];
				else if ($side == "height")
						return $tmpArray[1];
				else
						return FALSE;
			}
	
	/**
     * Funzione per raccogliere le informazioni essenziali da un array
     *
     * @param  array
     * @return array
     * 
     */	
			function getFromArray ( $tag, $array ) {
				
				foreach ( $array as $key => $value ) 
				{
					$return[] = $value[$tag];
				}
				
				return $return;
			}
		
	/**
     * Funzione per controllare che tutti i campi siano riempiti
     *
     * @param array $client_ay Array contenente le informazioni sul client
     * @return array Contenente l'array pulito e riempito
     */	
			function checkFaults ($client_ay) {
				
				$tmp_ay = array();
				foreach ($client_ay as $key => $value)
				{
					if (!empty($value)) {
							//*** Convert "Yes"/"No" to "TRUE"/"FALSE"
							if ($value == "Yes")
									$tmp_ay[$key] = TRUE;
							else if ($value == "No")
									$tmp_ay[$key] = FALSE;
							//*** This value is not boolean
							else 
									$tmp_ay[$key] = $value;
									
					//*** This value is empty, let's fill the free spaces
					} else {
							if ($key == "DEVICE_VERSION")
								$tmp_ay[$key] = "not versioned";
							elseif ($key == "DEVICE_MANUFACT")
								$tmp_ay[$key] = "none";
							elseif ($key == "DEVICE_MODEL")
								$tmp_ay[$key] = "none";
							elseif ($key == "JAVASCRIPT")
								$tmp_ay[$key] = ($this->_client_ay['DEVICE_TYPE'] == "browser") ? true : false;
							elseif ($key == "UPLOAD")
								$tmp_ay[$key] = ($this->_client_ay['DEVICE_TYPE'] == "browser") ? true : false;
							elseif ($key == "STREAMING")
								$tmp_ay[$key] = ($this->_client_ay['DEVICE_TYPE'] == "browser") ? true : false;
							else
								$tmp_ay[$key] = "unavailable";
						}
				}
				
				return $tmp_ay;
			}

	/**
     * Funzione per determinare le informazioni base sul device
     *
     * @return array 	'name' 			=> 'nome del device'
     * 				 				'manufact' 	=> 'manufacturer del device'
     * 				 				'model' 		=> 'modello del device'
     * 				 				'version' 	=> 'versione del device'
     * 				 				'type' 			=> 'tipo di device'
     */	
			function recognizeDevice () {
		
				$tmp_ay		= array();
				$def_ay		= array();
		
				// Camino
				if(preg_match('/mozilla.*rv:[0-9\.]+.*gecko\/[0-9]+.*camino\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']){
					$def_ay['name'] = "Camino";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// Netscape
				if(preg_match('/mozilla.*netscape[0-9]?\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Netscape";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// Epiphany
				if(preg_match('/mozilla.*gecko\/[0-9]+.*epiphany\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Epiphany";
					$def_ay['version']=($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// Galeon
				if(preg_match('/mozilla.*gecko\/[0-9]+.*galeon\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Galeon";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// Flock
				if(preg_match('/mozilla.*gecko\/[0-9]+.*flock\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Flock";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// Minimo
				if(preg_match('/mozilla.*gecko\/[0-9]+.*minimo\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Minimo";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// K-Meleon
				if(preg_match('/mozilla.*gecko\/[0-9]+.*k\-meleon\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "K-Meleon";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// K-Ninja
				if(preg_match('/mozilla.*gecko\/[0-9]+.*k-ninja\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "K-Ninja";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// Kazehakase
				if(preg_match('/mozilla.*gecko\/[0-9]+.*kazehakase\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Kazehakase";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// SeaMonkey
				if(preg_match('/mozilla.*rv:[0-9\.]+.*gecko\/[0-9]+.*seamonkey\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "SeaMonkey";
					$def_ay['version'] =($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// IceApe
				if(preg_match('/mozilla.*rv:[0-9\.]+.*gecko\/[0-9]+.*iceape\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "IceApe";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// Firefox
				if(preg_match('/mozilla.*rv:[0-9\.]+.*gecko\/[0-9]+.*Firefox\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Firefox";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// IceWeasel
				if(preg_match('/mozilla.*rv:[0-9\.]+.*gecko\/[0-9]+.*iceweasel\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "IceWeasel";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// Bon Echo
				if(preg_match('/mozilla.*rv:[0-9\.]+.*gecko\/[0-9]+.*BonEcho\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Bon Echo";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// Gran Paradiso
				if(preg_match('/mozilla.*rv:[0-9\.]+.*gecko\/[0-9]+.*GranParadiso\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Gran Paradiso";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// Minefield
				if(preg_match('/mozilla.*rv:[0-9\.]+.*gecko\/[0-9]+.*Minefield\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Minefield";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// Thunderbird
				if(preg_match('/mozilla.*rv:[0-9\.]+.*gecko\/[0-9]+.*thunderbird\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Thunderbird";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "mailbrowser";
				}
		
				// IceDove
				if(preg_match('/mozilla.*rv:[0-9\.]+.*gecko\/[0-9]+.*icedove\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "IceDove";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// Firebird
				if(preg_match('/mozilla.*rv:[0-9\.]+.*gecko\/[0-9]+.*firebird\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Firebird";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// Phoenix
				if(preg_match('/mozilla.*rv:[0-9\.]+.*gecko\/[0-9]+.*phoenix\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Phoenix";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// Mozilla Suite
				if(preg_match('/mozilla.*rv:([0-9\.]+).*gecko\/[0-9]+.*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Mozilla";
					$def_ay['type'] = "browser";
		
					if((int)substr($tmp_ay[1],2,1) <= 7)
						$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					else
						$def_ay['version'] = "compatible";
				}
		
				// Konqueror
				if(preg_match('/mozilla.*konqueror\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Konqueror";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
					if(preg_match('/khtml\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name'])
						$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
				}
		
				// Opera
				if((preg_match('/mozilla.*opera ([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) || preg_match('/^opera\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay))) {
					$def_ay['name'] = "Opera";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
					if(preg_match('/opera mini/si', $_SERVER['HTTP_USER_AGENT'])) {
						preg_match('/opera mini\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay);
						$def_ay['version'] .= " (Opera Mini" . ($tmp_ay[1] ? $tmp_ay[1] : "") . ")";
					}
				}
		
				// OmniWeb
				if(preg_match('/mozilla.*applewebkit\/[0-9]+.*omniweb\/v[0-9\.]+/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "OmniWeb";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// SunriseBrowser
				if(preg_match('/mozilla.*applewebkit\/[0-9]+.*sunrisebrowser\/([0-9a-z\+\-\.]+)/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "SunriseBrowser";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// DeskBrowse
				if(preg_match('/mozilla.*applewebkit\/[0-9]+.*deskbrowse\/([0-9a-z\+\-\.]+)/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "DeskBrowse";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// Shiira
				if(preg_match('/mozilla.*applewebkit.*shiira\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Shiira";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// Safari
				if(preg_match('/mozilla.*applewebkit.*safari\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Safari";
					$def_ay['type'] = "browser";
		
					if($tmp_ay[1] == "522.12" || $tmp_ay[1] == "522.13.1")
						$def_ay['version'] = " 3.0";
					elseif($tmp_ay[1] == "522.11.3")
						$def_ay['version'] = " 3.0 beta";
					elseif(substr($tmp_ay[1], 0, 3) == "419")
						$def_ay['version'] = " 2.0.4";
					elseif(substr($tmp_ay[1], 0, 3) == "417")
						$def_ay['version'] = " 2.0.3";
					elseif(substr($tmp_ay[1], 0, 3) == "416")
						$def_ay['version'] = " 2.0.2";
					elseif($tmp_ay[1] == "412.5")
						$def_ay['version'] = " 2.0.1";
					elseif(substr($tmp_ay[1], 0, 3) == "412")
						$def_ay['version'] = " 2.0";
					elseif($tmp_ay[1] == "312.6" || $tmp_ay[1] == "312.5")
						$def_ay['version'] = " 1.3.2";
					elseif(substr($tmp_ay[1], 0, 5) == "312.3")
						$def_ay['version'] = " 1.3.1";
					elseif(substr($tmp_ay[1], 0, 3) == "312")
						$def_ay['version'] = " 1.3";
					elseif($tmp_ay[1] == "125.12" || $tmp_ay[1] == "125.11")
						$def_ay['version'] = " 1.2.4";
					elseif($tmp_ay[1] == "125.9")
						$def_ay['version'] = " 1.2.3";
					elseif($tmp_ay[1] == "125.8" || $tmp_ay[1] == "125.7")
						$def_ay['version'] = " 1.2.2";
					elseif($tmp_ay[1] == "125.1")
						$def_ay['version'] = " 1.2.1";
					elseif(substr($tmp_ay[1], 0, 3) == "125")
						$def_ay['version'] = " 1.2";
					elseif($tmp_ay[1] == "101.1")
						$def_ay['version'] = " 1.1.1";
					elseif(substr($tmp_ay[1], 0, 3) == "100")
						$def_ay['version'] = " 1.1";
					elseif(substr($tmp_ay[1], 0, 4) == "85.8")
						$def_ay['version'] = " 1.0.3";
					elseif($tmp_ay[1] == "85.7")
						$def_ay['version'] = " 1.0.2";
					elseif(substr($tmp_ay[1], 0, 2) == "85")
						$def_ay['version'] = " 1.0";
					elseif(substr($tmp_ay[1], 0, 2) == "74")
						$def_ay['version'] = " 1.0b2";
					elseif(substr($tmp_ay[1], 0, 2) == "73")
						$def_ay['version'] = " 0.9";
					elseif(substr($tmp_ay[1], 0, 2) == "60")
						$def_ay['version'] = " 0.8.2";
					elseif(substr($tmp_ay[1], 0, 2) == "51")
						$def_ay['version'] = " 0.8.1";
					elseif(substr($tmp_ay[1], 0, 2) == "48")
						$def_ay['version'] = " 0.8";
				}
		
				// Dillo
				if(preg_match('/dillo\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Dillo";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
				}
		
				// iCab
				if(preg_match('/icab\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "iCab";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
				}
		
				// Lynx
				if(preg_match('/^lynx\/([0-9a-z\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Lynx";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "text";
				}
		
				// Links
				if(preg_match('/^links \(([0-9a-z\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Links";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "text";
				}
		
				// Elinks
				if(preg_match('/^elinks \(([0-9a-z\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "ELinks";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "leecher";
				}
				if(preg_match('/^elinks\/([0-9a-z\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "ELinks";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "leecher";
				}
				if(preg_match('/^elinks$/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "ELinks";
					$def_ay['type'] = "leecher";
				}
		
				// Wget
				if(preg_match('/^Wget\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Wget";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "leecher";
				}
		
				// Amiga Aweb
				if(preg_match('/Amiga\-Aweb\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Amiga Aweb";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
				}
		
				// Amiga Voyager
				if(preg_match('/AmigaVoyager\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Amiga Voyager";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
				}
		
				// QNX Voyager
				if(preg_match('/QNX Voyager ([0-9a-z.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "QNX Voyager";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
				}
		
				// IBrowse
				if(preg_match('/IBrowse\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "IBrowse";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
				}
		
				// Openwave
				if(preg_match('/UP\.Browser\/([0-9a-zA-Z\.]+).*/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Openwave";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
				}
				if(preg_match('/UP\/([0-9a-zA-Z\.]+).*/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Openwave";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
				}
		
				// NetFront
				if(preg_match('/NetFront\/([0-9a-z\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "NetFront";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
				}
		
				// PlayStation Portable
				if(preg_match('/psp.*playstation.*portable[^0-9]*([0-9a-z\.]+)\)/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "PSP";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "pda";
				}
		
				// Googlebot
				if(preg_match('/Googlebot\/([0-9a-z\+\-\.]+).*/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Googlebot";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "robot";
				}
		
				// Googlebot Image
				if(preg_match('/Googlebot\-Image\/([0-9a-z\+\-\.]+).*/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Googlebot Image ";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "robot";
				}
		
				// Gigabot
				if(preg_match('/Gigabot\/([0-9a-z\+\-\.]+).*/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Gigabot";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "robot";
				}
		
				// W3C Validator
				if(preg_match('/^W3C_Validator\/([0-9a-z\+\-\.]+)$/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "W3C Validator";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "robot";
				}
		
				// W3C CSS Validator
				if(preg_match('/W3C_CSS_Validator_[a-z]+\/([0-9a-z\+\-\.]+)$/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "W3C CSS Validator";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "robot";
				}
		
				// MSN Bot
				if(preg_match('/msnbot(-media|)\/([0-9a-z\+\-\.]+).*/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "MSNBot";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "robot";
				}
		
				// Psbot
				if(preg_match('/psbot\/([0-9a-z\+\-\.]+).*/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Psbot";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "robot";
				}
		
				// IRL crawler
				if(preg_match('/IRLbot\/([0-9a-z\+\-\.]+).*/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "IRL crawler";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "robot";
				}
		
				// Seekbot
				if(preg_match('/Seekbot\/([0-9a-z\+\-\.]+).*/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Seekport Robot";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "robot";
				}
		
				// Microsoft Research Bot
				if(preg_match('/^MSRBOT /s', $_SERVER['HTTP_USER_AGENT'])) {
					$def_ay['name'] = "MSRBot";
					$def_ay['type'] = "robot";
				}
		
				// cfetch/voyager
				if(preg_match('/^(cfetch|voyager)\/([0-9a-z\+\-\.]+)$/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "voyager";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "robot";
				}
		
				// BecomeBot
				if(preg_match('/BecomeBot\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "BecomeBot";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "robot";
				}
		
				// Alexa
				if(preg_match('/^ia_archiver$/s', $_SERVER['HTTP_USER_AGENT']) && !$def_ay['name']) {
					$def_ay['name'] = "Alexa";
					$def_ay['type'] = "robot";
				}
		
				// Inktomi Slurp
				if(preg_match('/Slurp.*inktomi/s', $_SERVER['HTTP_USER_AGENT']) && !$def_ay['name']) {
					$def_ay['name'] = "Inktomi Slurp";
					$def_ay['type'] = "robot";
				}
		
				// Yahoo Slurp
				if(preg_match('/Yahoo!.*Slurp/s', $_SERVER['HTTP_USER_AGENT']) && !$def_ay['name']) {
					$def_ay['name'] = "Yahoo! Slurp";
					$def_ay['type'] = "robot";
				}
		
				// Ask.com
				if(preg_match('/Ask Jeeves\/Teoma/s', $_SERVER['HTTP_USER_AGENT']) && !$def_ay['name']) {
					$def_ay['name'] = "Ask.com";
					$def_ay['type'] = "robot";
				}
		
				// MSIE
				if(preg_match('/microsoft.*internet.*explorer/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Microsoft Internet Explorer 1.0";
					$def_ay['type'] = "outdatedbrowser";
				}
				if(preg_match('/mozilla.*MSIE ([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Microsoft Internet Explorer";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
				// Netscape Navigator
				if(preg_match('/Mozilla\/([0-4][0-9\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Netscape Navigator";
					$def_ay['version'] = ($tmp_ay[1] ? $tmp_ay[1] : "");
					$def_ay['type'] = "browser";
				}
		
		
				if(preg_match('/mozilla/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name']) {
					$def_ay['name'] = "Mozilla compatible";
					$def_ay['type'] = "compatiblebrowser";
				}
		
				//*** CELLULARI
				
				// Nokia
				if(preg_match('/Nokia[ ]{0,1}([0-9a-zA-Z\+\-\.]+){0,1}.*/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name'])
				{
					if(!$def_ay['name'])
						$def_ay['name'] = "Nokia" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					else
						$def_ay['name'] .= " / Nokia" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					
					$def_ay['type'] = "mobile";
					$def_ay['manufact'] = "Nokia";
					$def_ay['model'] = $tmp_ay[1];
				}
		
				// Motorola
				if(preg_match('/mot\-([0-9a-zA-Z\+\-\.]+){0,1}\//si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name'])
				{
					if(!$def_ay['name'])
						$def_ay['name'] = "Motorola" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					else
						$def_ay['name'] .= " / Motorola" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					
					$def_ay['type'] = "mobile";
					$def_ay['manufact'] = "Motorola";
					$def_ay['model'] = $tmp_ay[1];
				}
		
				// Siemens
				if(preg_match('/sie\-([0-9a-zA-Z\+\-\.]+){0,1}\//si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name'])
				{
					if(!$def_ay['name'])
						$def_ay['name'] = "Siemens" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					else
						$def_ay['name'] .= " / Siemens" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					
					$def_ay['type'] = "mobile";
					$def_ay['manufact'] = "Siemens";
					$def_ay['model'] = $tmp_ay[1];
				}
		
				// Samsung
				if(preg_match('/samsung\-([0-9a-zA-Z\+\-\.]+){0,1}\//si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name'])
				{
					if(!$def_ay['name'])
						$def_ay['name'] = "Samsung" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					else
						$def_ay['name'] .= " / Samsung" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					
					$def_ay['type'] = "mobile";
					$def_ay['manufact'] = "Samsung";
					$def_ay['model'] = $tmp_ay[1];
				}
		
				// SonyEricsson & Ericsson
				if(preg_match('/SonyEricsson[ ]{0,1}([0-9a-zA-Z\+\-\.]+){0,1}.*/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name'])
				{
					if(!$def_ay['name'])
						$def_ay['name'] = "Sony Ericsson" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					else
						$def_ay['name'] .= " / Sony Ericsson" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					
					$def_ay['type'] = "mobile";
					$def_ay['manufact'] = "Sony Ericsson";
					$def_ay['model'] = $tmp_ay[1];
				}
				elseif(preg_match('/Ericsson[ ]{0,1}([0-9a-zA-Z\+\-\.]+){0,1}.*/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name'])
				{
					if(!$def_ay['name'])
						$def_ay['name'] = "Ericsson" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					else
						$def_ay['name'] .= " / Ericsson" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					
					$def_ay['type'] = "mobile";
					$def_ay['manufact'] = "Ericsson";
					$def_ay['model'] = $tmp_ay[1];
				}
		
				// Alcatel
				if(preg_match('/Alcatel\-([0-9a-zA-Z\+\-\.]+){0,1}.*/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name'])
				{
					if(!$def_ay['name'])
						$def_ay['name'] = "Alcatel" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					else
						$def_ay['name'] .= " / Alcatel" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					
					$def_ay['type'] = "mobile";
					$def_ay['manufact'] = "Alcatel";
					$def_ay['model'] = $tmp_ay[1];
				}
		
				// Panasonic
				if(preg_match('/Panasonic\-{0,1}([0-9a-zA-Z\+\-\.]+){0,1}.*/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name'])
				{
					if(!$def_ay['name'])
						$def_ay['name'] = "Panasonic" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					else
						$def_ay['name'] .= " / Panasonic" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					
					$def_ay['type'] = "mobile";
					$def_ay['manufact'] = "Panasonic";
					$def_ay['model'] = $tmp_ay[1];
				}
		
				// Philips
				if(preg_match('/Philips\-([0-9a-z\+\-\@\.]+){0,1}.*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name'])
				{
					if(!$def_ay['name'])
						$def_ay['name'] = "Philips" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					else
						$def_ay['name'] .= " / Philips" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					
					$def_ay['type'] = "mobile";
					$def_ay['manufact'] = "Philips";
					$def_ay['model'] = $tmp_ay[1];
				}
		
				// Acer
				if(preg_match('/Acer\-([0-9a-z\+\-\.]+){0,1}.*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name'])
				{
					if(!$def_ay['name'])
						$def_ay['name'] = "Acer" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					else
						$def_ay['name'] .= " / Acer" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					
					$def_ay['type'] = "mobile";
					$def_ay['manufact'] = "Acer";
					$def_ay['model'] = $tmp_ay[1];
				}
		
				// BlackBerry
				if(preg_match('/BlackBerry([0-9a-zA-Z\+\-\.]+){0,1}\//s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) && !$def_ay['name'])
				{
					if(!$def_ay['name'])
						$def_ay['name'] = "BlackBerry" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					else
						$def_ay['name'] .= " / BlackBerry" . ($tmp_ay[1] ? $tmp_ay[1] : "");
					
					$def_ay['type'] = "mobile";
					$def_ay['manufact'] = "BlackBerry";
					$def_ay['model'] = $tmp_ay[1];
				}
				
				return $def_ay;
			}
		
			/**
		     * Funzione per determinare il sistema usato dal client
		     *
		     * @return string
		     */	
			function recognizeSystem () {
		
				// Linux
				if(preg_match('/linux/si', $_SERVER['HTTP_USER_AGENT'])) {
					$system = "Linux";
		
					// Determino la distribuzione
					if(preg_match('/mdk/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (Mandrake)";
					elseif(preg_match('/kanotix/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (Kanotix)";
					elseif(preg_match('/lycoris/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (Lycoris)";
					elseif(preg_match('/knoppix/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (Knoppix)";
					elseif(preg_match('/centos/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (CentOS)";
					elseif(preg_match('/gentoo/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (Gentoo)";
					elseif(preg_match('/fedora/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (Fedora)";
					elseif(preg_match('/ubuntu.feist/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (Ubuntu 7.04 Feisty Fawn)";
					elseif(preg_match('/ubuntu.edgy/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (Ubuntu 6.10 Edgy Eft)";
					elseif(preg_match('/ubuntu.dapper/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (Ubuntu 6.06 LTS Dapper Drake)";
					elseif(preg_match('/ubuntu.breezy/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (Ubuntu 5.10 Breezy Badger)";
					elseif(preg_match('/kubuntu/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (Kubuntu)";
					elseif(preg_match('/xubuntu/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (Xubuntu)";
					elseif(preg_match('/ubuntu/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (Ubuntu)";
					elseif(preg_match('/slackware/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (Slackware)";
					elseif(preg_match('/suse/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (Suse)";
					elseif(preg_match('/redhat/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (Redhat)";
					elseif(preg_match('/debian/si', $_SERVER['HTTP_USER_AGENT']))
						$system .= " (Debian)";
					elseif(preg_match('/PLD\/([0-9.]*) \(([a-z]{2})\)/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay))
						$system .= " (PLD".($tmp_ay[1] ? " ".$tmp_ay[1] : "").($tmp_ay[2] ? " ".$tmp_ay[2] : "").")";
				}
		
				// BSD
				if(preg_match('/bsd/si', $_SERVER['HTTP_USER_AGENT']))
				{
						$system = "BSD";
					if(preg_match('/freebsd/si', $_SERVER['HTTP_USER_AGENT']))
						$system = "FreeBSD";
					elseif(preg_match('/openbsd/si', $_SERVER['HTTP_USER_AGENT']))
						$system = "OpenBSD";
					elseif(preg_match('/netbsd/si', $_SERVER['HTTP_USER_AGENT']))
						$system = "NetBSD";
						
					$this->_client_ay['DEVICE_TYPE']	= "browser";
				}
		
				// Mac OS (X)
				if((preg_match('/mac_/si', $_SERVER['HTTP_USER_AGENT']) || preg_match('/macos/si', $_SERVER['HTTP_USER_AGENT']) || preg_match('/powerpc/si', $_SERVER['HTTP_USER_AGENT']) || preg_match('/mac os/si', $_SERVER['HTTP_USER_AGENT']) || preg_match('/68k/si', $_SERVER['HTTP_USER_AGENT']) || preg_match('/macintosh/si', $_SERVER['HTTP_USER_AGENT'])))
				{
					$system = "Mac OS";
					$this->_client_ay['DEVICE_TYPE']	= "browser";
					
					if(preg_match('/mac os x/si', $_SERVER['HTTP_USER_AGENT']))
					{
						$system .= " X";
		
						if(preg_match('/applewebkit\/([0-9\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay))
						{
							if($tmp_ay[1] == "419.2.1")
								$system .= " 10.4.10";
							elseif($tmp_ay[1] == "419")
								$system .= " 10.4.9";
							elseif($tmp_ay[1] == "418.9.1")
								$system .= " 10.4.8";
							elseif($tmp_ay[1] == "418.9")
								$system .= " 10.4.8";
							elseif($tmp_ay[1] == "418.8")
								$system .= " 10.4.7";
							elseif(substr($tmp_ay[1], 0, 3) == "418")
								$system .= " 10.4.6";
							elseif($tmp_ay[1] == "417.9")
								$system .= " 10.4.4/10.4.5";
							elseif(substr($tmp_ay[1], 0, 3) == "416")
								$system .= " 10.4.3";
							elseif(substr($tmp_ay[1], 0, 4) == "412.")
								$system .= " 10.4.2";
							elseif(substr($tmp_ay[1], 0, 3) == "412")
								$system .= " 10.4/10.4.1";
							elseif(substr($tmp_ay[1], 0, 3) == "312")
								$system .= " 10.3.9";
							elseif($tmp_ay[1] == "125.5.7")
								$system .= " 10.3.8";
							elseif($tmp_ay[1] == "125.5.5" && $tmp_ay[2] == "125.11")
								$system .= " 10.3.6";
							elseif(($tmp_ay[1] == "125.5.6" || $tmp_ay[1] == "125.5.5") && substr($tmp_ay[1], 0, 5) == "125.1")
								$system .= " 10.3.6/10.3.7/10.3.8";
							elseif($tmp_ay[1] == "125.5" || $tmp_ay[1] == "125.4")
								$system .= " 10.3.5";
							elseif($tmp_ay[1] == "125.2")
								$system .= " 10.3.4";
							elseif($tmp_ay[1] == "100"  && $tmp_ay[2] == "100.1")
								$system .= " 10.3.2";
							elseif(substr($tmp_ay[1], 0, 3) == "100")
								$system .= " 10.3";
							elseif(substr($tmp_ay[1], 0, 2) == "85")
								$system .= " 10.2.8";
						}
					}
				}
		
				// ReactOS
				if(preg_match('/ReactOS ([0-9a-zA-Z\+\-\. ]+).*/s', $_SERVER['HTTP_USER_AGENT'], $tmp_ay))
					$system = "ReactOS" . ($tmp_ay[1] ? " ".$tmp_ay[1] : "");
		
				// SunOs
				if(preg_match('/sunos/si', $_SERVER['HTTP_USER_AGENT']))
					$system = "Solaris";
		
				// Amiga
				if(preg_match('/amiga/si', $_SERVER['HTTP_USER_AGENT']))
					$system = "Amiga";
		
				// Irix
				if(preg_match('/irix/si', $_SERVER['HTTP_USER_AGENT']))
					$system = "IRIX";
					
				// OpenVMS
				if(preg_match('/open.*vms/si', $_SERVER['HTTP_USER_AGENT']))
					$system = "OpenVMS";
		
				// BeOs
				if(preg_match('/beos/si', $_SERVER['HTTP_USER_AGENT']))
					$system = "BeOS";
		
				// QNX
				if(preg_match('/QNX/si', $_SERVER['HTTP_USER_AGENT']) && preg_match('/Photon/si', $_SERVER['HTTP_USER_AGENT']))
					$system = "QNX";
		
				// OS/2 Warp
				if(preg_match('/OS\/2.*Warp ([0-9.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay))
					$system = "OS/2 Warp" . ($tmp_ay[1] ? " ".$tmp_ay[1] : "");
		
				// Java
				if(preg_match('/j2me/si', $_SERVER['HTTP_USER_AGENT']))
				{
					$system = "Java Platform Micro Edition";
					$this->_client_ay['DEVICE_TYPE']	= "mobile";
				}
		
				// Symbian Os
				if(preg_match('/symbian/si', $_SERVER['HTTP_USER_AGENT']))
				{
					$system = "Symbian OS";
					$this->_client_ay['DEVICE_TYPE']	= "mobile";
		
					if(preg_match('/SymbianOS\/([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay))
					$system .= ($tmp_ay[1] ? " ".$tmp_ay[1] : "");
				}
		
				// Palm Os
				if(preg_match('/palmos/si', $_SERVER['HTTP_USER_AGENT']))
				{
					$system = "Palm OS";
					$this->_client_ay['DEVICE_TYPE']	= "pda";
		
					if(preg_match('/PalmOS ([0-9a-z\+\-\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay))
					$system .= ($tmp_ay[1] ? " ".$tmp_ay[1] : "");
				}
		
				// PlayStation Portable
				if(preg_match('/psp.*playstation.*portable/si', $_SERVER['HTTP_USER_AGENT']))
				{
					$system = "PlayStation Portable";
					$this->_client_ay['DEVICE_TYPE']	= "pda";
				}
		
				// Nintentdo Wii
				if(preg_match('/Nintendo Wii/si', $_SERVER['HTTP_USER_AGENT']))
				{
					$system = "Nintendo Wii";
					$this->_client_ay['DEVICE_TYPE']	= "consolle";
				}
		
		
				// Windows 3.x, 95, 98
				if(preg_match('/windows ([0-9\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay))
				{
					$system = "Windows" . ($tmp_ay[1] ? " ".$tmp_ay[1] : "");
					$this->_client_ay['DEVICE_TYPE']	= "browser";
				}
		
				if(preg_match('/[ \(]win([0-9\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay))
				{
					if($tmp_ay[1] == "16")
						$system = "Windows 3.x";
					elseif($tmp_ay[1] == "32")
						$system = "Windows";
					else
						$system = "Windows" . ($tmp_ay[1] ? " " . $tmp_ay[1] : "");
						
					$this->_client_ay['DEVICE_TYPE']	= "browser";
				}
		
				// Windows ME
				if(preg_match('/windows me/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) || preg_match('/win 9x 4\.90/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay))
				{
					$system = "Windows Millenium";
					$this->_client_ay['DEVICE_TYPE']	= "browser";
				}
		
				// Windows CE
				if(preg_match('/windows ce/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay))
				{
					$system = "Windows CE";
					$this->_client_ay['DEVICE_TYPE']	= "browser";
				}
		
				// Windows XP
				if(preg_match('/windows xp/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay))
				{
					$system = "Windows XP";
					$this->_client_ay['DEVICE_TYPE']	= "browser";
				}
		
				// Windows NT
				if(preg_match('/windows nt/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) || preg_match('/winnt/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay))
				{
					$system = "Windows NT";
					$this->_client_ay['DEVICE_TYPE']	= "browser";
				}
		
				// Windows NT
				if(preg_match('/windows nt ([0-9\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay) || preg_match('/winnt([0-9\.]+).*/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay))
				{
					if($tmp_ay[1] == "6.0")
						$system = "Windows Vista";
					elseif($tmp_ay[1] == "5.2")
						$system = "Windows Server 2003";
					elseif($tmp_ay[1] == "5.1")
						$system = "Windows XP";
					elseif($tmp_ay[1] == "5.0" || $tmp_ay[1] == "5.01")
						$system = "Windows 2000";
					else
						$system = "Windows NT" . ($tmp_ay[1] ? " ".$tmp_ay[1] : "");
						
					$this->_client_ay['DEVICE_TYPE']	= "browser";
				}
		
				if(preg_match('/windows/si', $_SERVER['HTTP_USER_AGENT'], $tmp_ay))
				{
					$system = "Windows";
					$this->_client_ay['DEVICE_TYPE']	= "browser";
				}
		
				return $system;
			}
}



//****************************************************************************
//****************************************************************************
//****************************************************************************
//****************************************************************************
//****************************************************************************
//****************************************************************************
//****************************************************************************
//****************************************************************************

$objHandle = new _client();
_dump($objHandle->gotStreaming());
?>